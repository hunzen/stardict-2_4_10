#ifndef __SD_LIB_H__
#define __SD_LIB_H__

#include "common.hpp"
#include "data.hpp"
#include "stddict.hpp"
#include "treedict.hpp"


#if defined(ARM) || defined(__sparc__)
static inline guint32 get_uint32(const gchar *addr)
{
	guint32 result;
	memcpy(&result, addr, sizeof(guint32));
	return result;
}
#else
#define get_uint32(x) *reinterpret_cast<const guint32 *>(x)
#endif

static inline gchar* stardict_datadup(gconstpointer mem)
{
#if (GLIB_MAJOR_VERSION == 2) && (GLIB_MINOR_VERSION >= 68)
        return (gchar *)g_memdup2(mem, sizeof(guint32) + *reinterpret_cast<const guint32 *>(mem));
#else
        return (gchar *)g_memdup(mem, sizeof(guint32) + *reinterpret_cast<const guint32 *>(mem));
#endif
}

typedef enum {
	qtSIMPLE, qtREGEXP, qtFUZZY, qtDATA
} query_t;
	
extern query_t analyse_query(const char *s, std::string& res); 

#endif//!__SD_LIB_H__
