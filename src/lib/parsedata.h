/*
 * Copyright 2011 kubtek <kubtek@mail.com>
 * Copyright 2025 Hu Zheng <huzheng_001@hotmail.com>
 *
 * This file is part of StarDict.
 *
 * StarDict is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * StarDict is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with StarDict.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef _STARDICT_PARSEDATA_H_
#define _STARDICT_PARSEDATA_H_

#include <gtk/gtk.h>
#include <string>
#include <list>

enum ParseResultItemType {
	ParseResultItemType_mark,
};

struct ParseResultMarkItem {
	std::string pango;
};

struct ParseResultItem {
	ParseResultItemType type;
	union {
		ParseResultMarkItem *mark;
	};
};

struct ParseResult {
	~ParseResult();
	void clear();
	std::list<ParseResultItem> item_list;
};

#endif
