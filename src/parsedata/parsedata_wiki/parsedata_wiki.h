#ifndef PARSEDATA_WIKI_H
#define PARSEDATA_WIKI_H

#include <string>
#include <gtk/gtk.h>

extern std::string wiki2pango(const char *p, guint32 sec_size);

#endif //PARSEDATA_WIKI_H
