/*
 * Copyright 2011 kubtek <kubtek@mail.com>
 * Copyright 2025 Hu Zheng <huzheng_001@hotmail.com>
 *
 * This file is part of StarDict.
 *
 * StarDict is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * StarDict is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with StarDict.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "parsedata_wiki.h"
#include "stardict_wiki2xml.h"
#include <cstring>
#include <glib/gi18n.h>

#ifdef _WIN32
#include <windows.h>
#endif

std::string wiki2pango(const char *p, guint32 sec_size)
{
	std::string res(p, sec_size);
	std::string xml = wiki2xml(res);
	std::string pango = wikixml2pango(xml);
	return pango;
}

