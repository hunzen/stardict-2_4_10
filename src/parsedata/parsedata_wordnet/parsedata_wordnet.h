#ifndef PARSEDATA_WORDNET_H
#define PARSEDATA_WORDNET_H

#include <string>
#include <gtk/gtk.h>

extern std::string wordnet2pango(const char *p, guint32 sec_size, const char *oword);

#endif //PARSEDATA_WORDNET_H
