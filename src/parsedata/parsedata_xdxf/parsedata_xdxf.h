#ifndef PARSEDATA_XDXF_H
#define PARSEDATA_XDXF_H

#include <string>
#include <gtk/gtk.h>

extern std::string xdxf2pango(const char *p, guint32 sec_size, const char *oword);

#endif //PARSEDATA_XDXF_H
