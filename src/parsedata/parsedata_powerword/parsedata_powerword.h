#ifndef PARSEDATA_POWERWORD_H
#define PARSEDATA_POWERWORD_H

#include <string>
#include <gtk/gtk.h>

extern std::string powerword2pango(const char *p, guint32 sec_size, const char *oword);

#endif //PARSEDATA_POWERWORD_H
