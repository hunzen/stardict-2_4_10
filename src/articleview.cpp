/* 
 * This file part of StarDict - A international dictionary for GNOME.
 * http://stardict-4.sourceforge.net
 * Copyright (C) 2005 Evgeniy <dushistov@mail.ru>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Library General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#ifdef HAVE_CONFIG_H
#  include "config.h"
#endif

#include <cstring>
#include <gtk/gtk.h>

#include "articleview.h"
#include "parsedata/parsedata_powerword/parsedata_powerword.h"
#include "parsedata/parsedata_wiki/parsedata_wiki.h"
#include "parsedata/parsedata_xdxf/parsedata_xdxf.h"


void ArticleView::AppendData(gchar *data, const gchar *oword)
{  
  std::string mark;

  guint32 data_size,sec_size=0;
  const gchar *p=data;
  data_size=*reinterpret_cast<const guint32 *>(p);
  p+=sizeof(guint32);
  bool first_time = true;
  while (guint32(p - data)<data_size) {
    if (first_time)
      first_time=false;
    else
      mark+= "\n";
    switch (*p) {
    case 'm':
    //case 'l'://need more work... TODO: convert from local encoding to utf-8
      p++;
      sec_size = strlen(p);
      if (sec_size) {
	gchar *m_str = g_markup_escape_text(p, sec_size);
	mark+=m_str;
	g_free(m_str);
      }
      sec_size++;
      break;
    case 'g':
      p++;
      sec_size=strlen(p);
      if (sec_size) {
	//AppendPangoText(p);
	mark+=p;
      }
      sec_size++;
      break;
    case 'x':
      p++;
      sec_size = strlen(p);
      if (sec_size) {
	std::string res=xdxf2pango(p, sec_size, oword);
	mark+=res;
      }
      sec_size++;
      break;
    case 'k':
      p++;
      sec_size = strlen(p);
      if (sec_size) {
        std::string res=powerword2pango(p, sec_size, oword);
        mark+=res;
      }
      sec_size++;
      break;
    case 'w':
      p++;
      sec_size = strlen(p);
      if (sec_size) {
        std::string res=wiki2pango(p, sec_size);
        mark+=res;
      }
      sec_size++;
      break;
    case 'h':
      p++;
      sec_size = strlen(p);
      if (sec_size) {
	std::string res=html2pango(p);
	mark+=res;
      }
      sec_size++;
      break;
    case 'n':
      p++;
      sec_size = strlen(p);
      if (sec_size) {
        std::string res=wordnet2pango(p, sec_size, oword);
        mark+=res;
      }
      sec_size++;
      break;
    case 't':
      p++;
      sec_size = strlen(p);
      if (sec_size) {
	mark += "[<span foreground=\"blue\">";
	gchar *m_str = g_markup_escape_text(p, sec_size);
	mark += m_str;
	g_free(m_str);
	mark += "</span>]";
      }
      sec_size++;						
      break;
    case 'y':
      p++;
      sec_size = strlen(p);
      if (sec_size) {
	mark += "[<span foreground=\"red\">";
	gchar *m_str = g_markup_escape_text(p, sec_size);
	mark += m_str;
	g_free(m_str);
	mark += "</span>]";
      }
      sec_size++;
      break;
    /*case 'r':
	p++;
	sec_size = strlen(p);
	if(sec_size) {
		append_and_mark_orig_word(mark, real_oword, LinksPosList());
		mark.clear();
		append_resource_file_list(p);
	}
	sec_size++;
	break;*/
    case 'W':
      p++;
      sec_size=g_ntohl(*reinterpret_cast<const guint32 *>(p));
      //enbale sound button.
      sec_size += sizeof(guint32);
      break;
    case 'P':
      p++;
      sec_size=g_ntohl(*reinterpret_cast<const guint32 *>(p));
      //show this picture.
      sec_size += sizeof(guint32);
      break;
    default:
      if (g_ascii_isupper(*p)) {
        p++;
        sec_size=g_ntohl(*reinterpret_cast<const guint32 *>(p));
        sec_size += sizeof(guint32);
      } else {
        p++;
        sec_size = strlen(p)+1;
      }
      mark += _("Unknown data type, please upgrade StarDict!");
      break;
    }
    p += sec_size;
  }
  AppendPangoText(mark.c_str());
}

void ArticleView::AppendNewline()
{
	AppendPangoText("\n");
}

void ArticleView::AppendDataSeparate()
{
	AppendPangoText("\n");
}

void ArticleView::AppendHeader(const std::string& dict_name, int i)
{
	if (!for_float_win) {
		gchar *mark = g_strdup_printf("%d", i);
		AppendMark(mark);
		g_free(mark);
	}
	std::string mark= "<span foreground=\"blue\">";
#ifdef CONFIG_GPE
	mark+= "&lt;- ";
#else
	mark+= "&lt;--- ";
#endif
	gchar *m_str = g_markup_escape_text(dict_name.c_str(), -1);
	mark += m_str;
	g_free(m_str);
#ifdef CONFIG_GPE
	mark += " -&gt;</span>\n";
#else
	mark += " ---&gt;</span>\n";
#endif
	AppendPangoText(mark.c_str());
}

void ArticleView::AppendWord(const gchar *word)
{
	std::string mark;
	mark += for_float_win ? "<span foreground=\"purple\">" : "<b><span size=\"x-large\">";
	gchar *m_str = g_markup_escape_text(word, -1);
	mark += m_str;
	g_free(m_str);
	mark += for_float_win ? "</span>\n" : "</span></b>\n";
	AppendPangoText(mark.c_str());
}
